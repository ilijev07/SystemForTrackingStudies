<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['web']], function () {

    Route::post('/register', ['uses' => 'Auth\AuthController@register',]);

    Route::post('/signin', ['uses' => 'Auth\AuthController@signin']);

    Route::post('/update',['uses' => 'UserController@update']);

    Route::delete('/delete',['uses' => 'UserController@delete']);

    Route::post('/addsubject', ['uses' => 'SubjectController@addsubject']);

    Route::get('/getPassedSubjects',['uses' => 'SubjectController@getPassedSubjects']);

    Route::get('/getNotPassedSubjects',['uses' => 'SubjectController@getNotPassedSubjects']);

    Route::delete('/deleteSubject',['uses' => 'SubjectController@delete']);

    Route::get('/getSubject', ['uses' => 'SubjectController@getSubject']);

    Route::post('/updateSubject', ['uses' => 'SubjectController@updateSubject']);

});
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/user', [
        'uses' => 'UserController@index',
    ]);
});




