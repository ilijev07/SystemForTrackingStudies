<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',120);
            $table->string('professor',120);
            $table->enum('type',['mandatory','elective'])->default('mandatory');
            $table->integer('semester')->unsigned();
            $table->integer('theoryPoints')->unsigned()->default(0);
            $table->integer('projectPoints')->unsigned()->default(0);
            $table->integer('practicallyPoints')->unsigned()->default(0);
            $table->integer('laboratoryPoints')->unsigned()->default(0);
            $table->integer('totalPoints')->unsigned()->default(100);
            $table->integer('grade')->unsigned()->default(5);
            $table->integer('credits')->unsigned()->default(6);

            $table->integer('user_id')->unsigned();

            $table->timestamps();
        });
        Schema::table('subjects', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
