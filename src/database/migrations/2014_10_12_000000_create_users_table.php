<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',120);
            $table->string('surname',120);
            $table->string('email',60)->unique();
            $table->string('index',60)->unique();
            $table->string('studyProgram',20);
            $table->integer('totalCredits')->unsigned()->default(0);
            $table->double('average')->default(5);
            $table->string('password',60);
            $table->rememberToken();
            $table->timestamps();
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
