/**
 * Created by Ilija on 29-Jan-17.
 */


var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

elixir(function(mix) {
    mix
        .sass('app.scss')
        .browserify('auth.js')
        .browserify('app.js');
});