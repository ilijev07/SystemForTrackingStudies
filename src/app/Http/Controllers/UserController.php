<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UpdateRequest;

use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        $data['name'] = $request->user()->name;
        $data['email'] = $request->user()->email;
        return response()->json([
            'data' => $request->user(),
        ]);
    }
    public function update(UpdateRequest $request)
    {
        $user=User::find($request->json('id'));
        $user->name=$request->json('name');
        $user->surname=$request->json('surname');
        $user->index=$request->json('index');
        $user->studyProgram=$request->json('studyProgram');
        $user->email=$request->json('email');
        if(!Hash::check($request->json('password'),$user->password))
        {
            return response()->json([
                'error' => 'Password is not matching',
            ], 401);
        }
        else
            $user->password=bcrypt($request->json('password1'));
        $user->save();
    }
    public function delete(Request $request)
    {

        User::destroy($request->json('id'));
    }
}
