<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateSubjectFormRequest;
use Illuminate\Http\Request;
use App\Http\Requests\AddSubjectFormRequest;
use App\Subject;
use App\User;


class SubjectController extends Controller
{
    //



    public function addsubject(AddSubjectFormRequest $request)
    {

        $user=User::find($request->json('id'));

        $subject=new Subject;
        $subject->name=$request->json('name');
        $subject->professor=$request->json('professor');
        $subject->type=$request->json('type');

        $subject->semester=$request->json('semester');
        $subject->grade=$request->json('grade');

        $subject->credits=$request->json('credits');




        $subject->theoryPoints=$request->json('theoryPoints');

        $subject->practicallyPoints=$request->json('practicallyPoints');
        $subject->laboratoryPoints=$request->json('laboratoryPoints');
        $subject->projectPoints=$request->json('projectPoints');

        $subject->totalPoints=$request->json('totalPoints');
        $user->subjects()->save($subject);


        if(intval($request->json('grade'))>5)
        {
            $numberOfSubjects=0;
            $sum=0;
            $cred=0;
            foreach($user->subjects as $sub)
            {
                if($sub->grade>5)
                {
                    $sum+=$sub->grade;
                    $cred+=$sub->credits;
                    $numberOfSubjects++;
                }

            }
            $avg=doubleval($sum/$numberOfSubjects);
            $user->average=$avg;
            $user->totalCredits=$cred;

        }
        $user->save();


    }

    public function getPassedSubjects(Request $request)
    {
        $user=User::find($request->id);

        $subjects=$user->subjects;
        $subjects=$subjects->filter(function($subject)
        {
            return $subject->grade>5;
        });


        return response()->json([
            'data' => $subjects,
        ]);
    }

    public function getNotPassedSubjects(Request $request)
    {
        $user=User::find($request->id);

        $subjects=$user->subjects;
        $subjects=$subjects->filter(function($subject)
        {
            return $subject->grade==5;
        });
        $creditsToGet=$subjects->count()*6;

        return response()->json([
            'data' => $subjects,
            'creditsToGet' =>$creditsToGet,
        ]);
    }

    public function delete(Request $request)
    {

        $subject=Subject::find($request->json('id'));
        $user=User::find($request->json('userID'));
        if($subject->grade>5)
        {
            $totalSubjects=$user->subjects->count();
            $sum=$totalSubjects*$user->average;
            $sum-=$subject->grade;
            $totalSubjects-=1;
            $user->totalCredits-=$subject->credits;

            if($totalSubjects==0)
                $user->average=0;

            else
                $user->average=doubleval($sum/$totalSubjects);
        }

        Subject::destroy($request->json('id'));
        $user->save();
    }
    public function getSubject(Request $request)
    {

        $user=User::find($request->id);
        $subjectID=$request->subjectID;
        $subject=null;
        $subjects=$user->subjects;
        foreach($subjects as $sub)
        {
            if($sub->id==$subjectID)
            {
                $subject=$sub;
                break;
            }
        }

        return response()->json([
            'data' => $subject,
        ]);

    }

    public function updateSubject(UpdateSubjectFormRequest $request)
    {
        $user=User::find($request->json('id'));

        $subject=Subject::find($request->json('subjectID'));



        $totalSubjects=0;

        $sum=0;
        $cred=0;
        foreach($user->subjects as $sub)
        {
            if($sub->grade>5)
            {
                $sum+=$sub->grade;
                $cred+=$sub->credits;
                $totalSubjects+=1;
            }

        }
        $user->totalCredits=$cred;

        if($subject->grade>5 && intval($request->json('grade'))<=5)
        {
            $sum-=$subject->grade;
            $totalSubjects-=1;
            $user->average=doubleval($sum/($totalSubjects));
            $user->totalCredits-=$subject->credits;
            $user->save();
        }

        else if($subject->grade<=5 && intval($request->json('grade'))>5)
        {

            $sum+=intval($request->json('grade'));
            $totalSubjects+=1;
            $user->average=doubleval($sum/($totalSubjects));
            $user->totalCredits+=intval($request->json('credits'));
            $user->save();
        }
        else  if($subject->grade>5 && intval($request->json('grade'))>5)
        {
            $sum-=$subject->grade;
            $sum+=$request->json('grade');

            $user->average=doubleval($sum/($totalSubjects));
            $user->totalCredits-=$subject->credits;
            $user->totalCredits+=intval($request->json('credits'));
            $user->save();
        }

        $subject->name=$request->json('name');
        $subject->professor=$request->json('professor');
        $subject->type=$request->json('type');

        $subject->semester=$request->json('semester');
        $subject->grade=$request->json('grade');

        $subject->credits=$request->json('credits');




        $subject->theoryPoints=$request->json('theoryPoints');

        $subject->practicallyPoints=$request->json('practicallyPoints');
        $subject->laboratoryPoints=$request->json('laboratoryPoints');
        $subject->projectPoints=$request->json('projectPoints');

        $subject->totalPoints=$request->json('totalPoints');


        $user->subjects()->save($subject);

        $user->save();



    }


}
