<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterFormRequest;
use Carbon\Carbon;
use JWTAuth;


use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;



class AuthController extends Controller
{
    public function register(RegisterFormRequest $request)
    {
        if($request->json('password')==$request->json('password1'))
        {
            User::create([
                'name' => $request->json('name'),
                'surname' => $request->json('surname'),
                'index' => $request->json('index'),
                'studyProgram' => $request->json('studyProgram'),
                'email' => $request->json('email'),

                'password' => bcrypt($request->json('password')),
            ]);
        }
        else
            return response()->json([
                'error' => 'Password and Confirm password field are not equal',
            ], 401);

    }
    public function signin(Request $request)
    {
        try {
            $token = JWTAuth::attempt($request->only('email', 'password'), [
                'exp' => Carbon::now()->addWeek()->timestamp,
            ]);
        } catch (JWTException $e) {
            return response()->json([
                'error' => 'Could not authenticate',
            ], 500);
        }

        if (!$token) {
            return response()->json([
                'error' => 'Could not authenticate',
            ], 401);
        } else {
            $data = [];
            $meta = [];

            $data['name'] = $request->user()->name;
            $data['id']=$request->user()->id;
            $meta['token'] = $token;

            return response()->json([
                'data' => $data,
                'meta' => $meta
            ]);
        }
    }


}