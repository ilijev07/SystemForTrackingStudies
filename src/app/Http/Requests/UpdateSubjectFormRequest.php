<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubjectFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'subjectID' => 'required',
            'name' => 'required',
            'professor'=>'required',
            'type' => 'required|in:mandatory,elective',
            'semester'=>'required|numeric|min:1,max:8',
            'grade'=>'required|numeric|min:5,max:10',
            'credits' => 'required|numeric|min:1',
            'theoryPoints' => 'required|numeric|min:0',
            'practicallyPoints' =>'required|numeric|min:0',
            'laboratoryPoints' => 'required|numeric|min:0',
            'projectPoints'=>'required|numeric|min:0',
            'totalPoints' =>'required|numeric|min:0'
        ];
    }
}
