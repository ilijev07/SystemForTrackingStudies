<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    //
    protected $fillable = [
        'name','professor','grade','credits','theoryPoints','practicallyPoints', 'laboratoryPoints', 'projectPoints'
        ,'totalPoints','type'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
