var Vue = require('vue');
var VueRouter = require('vue-router');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.getElementsByName('csrf-token')[0].getAttribute('content');
Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');
Vue.http.options.root = 'http://localhost:8000';

import auth from './auth.js';
import App from './components/App.vue';

import Home from './components/Home.vue';
import Register from './components/Register.vue';
import EditProfile from './components/EditProfile.vue';
import NotFound from './components/NotFound.vue';
import Signin from './components/Signin.vue';
import AddSubject from './components/AddSubject.vue';
import PassedSubjects from './components/PassedSubjects.vue';
import NotPassedSubjects from './components/NotPassedSubjects.vue';

import EditSubject from './components/EditSubject.vue';

Vue.use(VueRouter);

export default Vue;
export var router = new VueRouter;

router.map({
    '/': {
        name: 'home',
        component: Home
    },
    '/register': {
        name: 'register',
        component: Register
    },
    '/signin': {
        name: 'signin',
        component: Signin,
    },
    '/editprofile':{
        name:'editprofile',
        component: EditProfile,
        auth:true,
    },
     'addsubject':{
         name:'addsubject',
         component: AddSubject,
         auth:true,
    },
    'passedsubjects':{
        name:'passedsubjects',
        component: PassedSubjects,
        auth:true,
    },
    'notpassedsubjects':{
        name:'notpassedsubjects',
        component: NotPassedSubjects,
        auth:true,
    },
    'editsubject/:id':{
        name:'editsubject',
        component: EditSubject,
        auth:true,
    }

});
router.start(App, '#app');
// Redirect to the home route if any routes are unmatched
router.redirect({
    '*': {
        component:NotFound
    }
});



router.beforeEach(function (transition) {

    if (transition.to.auth && !auth.user.authenticated) {

        // if route requires auth and user isn't authenticated
        console.log("User beforeEach "+auth.user.profile);
        localStorage.setItem('user',auth.user.profile);


        transition.redirect('/signin')
    } else {
        transition.next()
    }
});




