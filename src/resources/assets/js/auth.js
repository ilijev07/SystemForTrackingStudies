/**
 * Created by Ilija on 03-Feb-17.
 */

import Vue from './app.js';

import {router} from './app.js';


export default {
    user:
    {
        authenticated: false,
        profile: null


    },
    passedSubjects:[],
    notPassedSubjects:[],

    check1(context) {
        if (localStorage.getItem('id_token') !== null) {
            Vue.http.get(
                '/user',
            ).then(response => {


                context.auth.user.authenticated = true;
                context.auth.user.profile = response.data.data;

            })
        }
    },
    check() {
        if (localStorage.getItem('id_token') !== null) {
            Vue.http.get(
                '/user',
            ).then(response => {


                this.user.authenticated = true;
                this.user.profile = response.data.data;

            })
        }
    },

    getAuthHeader() {
        return {
            'Authorization': 'Bearer ' + localStorage.getItem('id_token')
        }
    },
    getCsrfHeader(){
        return {
            'X-CSRF-TOKEN':document.getElementsByName('csrf-token')[0].getAttribute('content')
        }
    },

    addSubject(context,id,name,professor,type,semester,theoryPoints,practicallyPoints,laboratoryPoints,projectPoints,totalPoints,grade,credits) {
        Vue.http.post(
            '/addsubject',
            {
                id:id,
                name: name,
                professor:professor,
                type:type,
                semester:semester,
                theoryPoints:theoryPoints,
                practicallyPoints: practicallyPoints,
                laboratoryPoints: laboratoryPoints,
                projectPoints:projectPoints,
                totalPoints:totalPoints,
                grade:grade,
                credits:credits
            }
        ).then(response => {
            context.success = true;
        }, response => {
            context.response = response.data;
            context.error = true
        })
    },
    updateSubject(context,id,subjectID,name,professor,type,semester,theoryPoints,practicallyPoints,laboratoryPoints,projectPoints,totalPoints,grade,credits) {
        Vue.http.post(
            '/updateSubject',
            {
                id:id,
                subjectID:subjectID,
                name: name,
                professor:professor,
                type:type,
                semester:semester,
                theoryPoints:theoryPoints,
                practicallyPoints: practicallyPoints,
                laboratoryPoints: laboratoryPoints,
                projectPoints:projectPoints,
                totalPoints:totalPoints,
                grade:grade,
                credits:credits
            }
        ).then(response => {
            context.success = true;
        }, response => {
            context.response = response.data;
            context.error = true
        })
    },
    deleteStudent(context,id) {
        Vue.http.delete(
            '/delete',
            {
                id: id,

            },
        ).then(response => {
            context.success = true;
            localStorage.removeItem('id_token');
            router.go({
                name: 'home'
            })
        }, response => {
            context.response = response.data;
            context.error = true
        })
    },
    register(context, name, surname, index, studyProgram, email, password,password1) {
        Vue.http.post(
            '/register',
            {
                name: name,
                surname:surname,
                index:index,
                studyProgram:studyProgram,
                email: email,
                password: password,
                password1:password1
            }
        ).then(response => {
            context.success = true
        }, response => {
            context.response = response.data;
            context.error = true
        })
    },
    update(context,id, name, surname, index, studyProgram, email, password,password1) {
        Vue.http.post(
            '/update',
            {
                id:id,
                name: name,
                surname:surname,
                index:index,
                studyProgram:studyProgram,
                email: email,
                password: password,
                password1:password1
            }
        ).then(response => {
            context.success = true
        }, response => {
            context.response = response.data;
            context.error = true
        })
    },
    signin(context, email, password) {
        Vue.http.post(
            '/signin',
            {
                email: email,
                password: password
            }
        ).then(response => {
            context.error = false;
            localStorage.setItem('id_token', response.data.meta.token);
            Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');

            this.user.authenticated = true;
            this.user.profile = response.data.data;
            console.log(this.user.profile);
            router.go({
                name: 'home'
            })
        }, response => {
            context.error = true
        })
    },
    signout() {
        localStorage.removeItem('id_token');
        this.user.authenticated = false;
        this.user.profile = null;

        router.go({
            name: 'home'
        })
    }


}

